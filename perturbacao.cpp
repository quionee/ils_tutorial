#include <iostream>
#include <fstream>
#include "perturbacao.hpp"
#include "buscaLocal.hpp"

Perturbacao::Perturbacao(std::string tipoPerturbacao, std::string buscaLocalPer, std::string 
                         buscaLocalPerProx, int qtdIteracoes) {
    this->tipoPerturbacao = tipoPerturbacao;
    this->buscaLocalPer = buscaLocalPer;
    this->buscaLocalPerProx = buscaLocalPerProx;
    this->qtdIteracoes = qtdIteracoes;
}


Perturbacao::~Perturbacao() {
    
}

std::vector<Grupo> Perturbacao::perturbacao(Grafo *grafo, std::vector<Grupo> &solucao,
                                            BuscaLocal &buscaLocal) {
    if (tipoPerturbacao == "weakPer") {
        return perturbacaoFraca(grafo, solucao, buscaLocal);
    }
    else {
        return perturbacaoForte(grafo, solucao, buscaLocal);
    }

}

std::vector<Grupo> Perturbacao::realizaBuscaLocal(BuscaLocal &busca, Grafo *&grafo,
                                                  std::vector<Grupo> solucao) {
    if (buscaLocalPer == "insertionPer") {
        return busca.insercaoSemValorDoMovimento(grafo, solucao);
    }
    else {
        return busca.swapSemValorDoMovimento(grafo, solucao);
    }
}

std::vector<Grupo> Perturbacao::realizaBuscaLocalProx(BuscaLocal &busca, Grafo *&grafo,
                                                  std::vector<Grupo> solucao) {
    if (buscaLocalPerProx == "insertionPer") {
        return busca.insercaoSemValorDoMovimento(grafo, solucao);
    }
    else if (buscaLocalPerProx == "swapPer") {
        return busca.swapSemValorDoMovimento(grafo, solucao);
    }
    else if (buscaLocalPerProx == "threeChainPer") {
        return busca.swapEmCadeiaSemValorDoMovimento(grafo, solucao);
    }
    else {
        return solucao;
    }
}

// método de perturbação fraca com possibilidade das três buscas locais.
std::vector<Grupo> Perturbacao::perturbacaoFraca(Grafo* grafo, std::vector<Grupo> &s0,
                                                 BuscaLocal &buscaLocal) {
    std::vector<Grupo> sp = s0;
    std::vector<Grupo> s;
    std::vector<Grupo> sLinha;
    // srand(time(NULL));

    for (int i = 0; i < qtdIteracoes; ++i) {
        s = realizaBuscaLocal(buscaLocal, grafo, sp);
        s = realizaBuscaLocalProx(buscaLocal, grafo, s);
        
        for (int j = 0; j < grafo->getQtdElementos(); ++j) {
            sLinha = realizaBuscaLocal(buscaLocal, grafo, sp);
            sLinha = realizaBuscaLocalProx(buscaLocal, grafo, sLinha);
            
            double somatorioS = 0, somatorioS1 = 0;
            for (int i = 0; i < grafo->getQtdGrupos(); ++i) {
                somatorioS += s[i].getSomatorioDistancias();
                somatorioS1 += sLinha[i].getSomatorioDistancias();
            }
            
            if (somatorioS1 > somatorioS) {
                s = sLinha;
            }
        }
        sp = s;
    }

    return sp;
}


// método de perturbação forte com possibilidade das três buscas locais.
std::vector<Grupo> Perturbacao::perturbacaoForte(Grafo* grafo, std::vector<Grupo> &s0, 
                                                 BuscaLocal &buscaLocal) {
    // srand(time(0));
    
    std::vector<Grupo> sp = s0;
    std::vector<Grupo> s;

    for (int i = 0; i < qtdIteracoes; ++i) {
        s = realizaBuscaLocal(buscaLocal, grafo, sp);
        s = realizaBuscaLocalProx(buscaLocal, grafo, s);

        sp = s;
    }
    return sp;
}
