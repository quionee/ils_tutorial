# name  switch  type  values  [conditions (using R syntax)]

ini_sol "--inisol " c (lh, gc, wj)
perturbation "--per " c (weakPer, strongPer)
local_search "--ls " c (insertion, swap)
ls_insertion "--lsin " c (swap, threeChain, none) | local_search == "insertion"
ls_swap "--lssw " c (insertion, threeChain, none) | local_search == "swap"
local_search_per "--lsper " c (insertionPer, swapPer)
ls_insertion_per "--lsinper " c (swapPer, threeChainPer, none) | local_search_per == "insertionPer"
ls_swap_per "--lsswper " c (insertionPer, threeChainPer, none) | local_search_per == "swapPer"
acceptance "--accept " c (acceptAlways, acceptBetter, acceptBetterEqual)
