# Uma Introdução à Configuração Automática de Algoritmos
---
## Lorena Tavares, Mayron César O. Moreira
---
#### Universidade Federal de Lavras (UFLA)
#### Departamento de Ciência da Computação
#### Lavras, Minas Gerais, Brasil 

### Configuração Automática de Algoritmos
O código foi adaptado para que fosse configurado automaticamente pelo pacote IRACE (Iterated Race for Automatic Algorithm Configuration).

### Gerando o executável para o uso pelo Irace:

* dentro da pasta do projeto, executar o comando `make` pelo terminal

