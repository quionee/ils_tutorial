#ifndef PERTUBACAO_HPP
#define PERTUBACAO_HPP

#include <vector>
#include <cstdlib>
#include <ctime>
#include "grafo.hpp"
#include "grupo.hpp"
#include "buscaLocal.hpp"

class Perturbacao {
    private:
        std::string tipoPerturbacao;
        std::string buscaLocalPer;
        std::string buscaLocalPerProx;
        int qtdIteracoes;
    public:
        Perturbacao(std::string tipoPerturbacao, std::string buscaLocalPer,
                    std::string buscaLocalPerProx, int qtdIteracoes);
        ~Perturbacao();

        std::vector<Grupo> perturbacao(Grafo *grafo, std::vector<Grupo> &solucao,
                                       BuscaLocal &buscaLocal);

        std::vector<Grupo> perturbacaoFracaUmaBuscaLocal(Grafo* grafo, std::vector<Grupo> &solucao, 
                                                         BuscaLocal &buscaLocal);
        std::vector<Grupo> perturbacaoForteUmaBuscaLocal(Grafo* grafo, std::vector<Grupo> &solucao, 
                                                         BuscaLocal &buscaLocal);

        std::vector<Grupo> realizaBuscaLocal(BuscaLocal &busca, Grafo *&grafo,
                                             std::vector<Grupo> solucao);
        std::vector<Grupo> realizaBuscaLocalProx(BuscaLocal &busca, Grafo *&grafo,
                                             std::vector<Grupo> solucao);

        std::vector<Grupo> perturbacaoFraca(Grafo* grafo, std::vector<Grupo> &solucao,
                                            BuscaLocal &buscaLocal);
        std::vector<Grupo> perturbacaoForte(Grafo* grafo, std::vector<Grupo> &solucao,
                                            BuscaLocal &buscaLocal);
};

#endif
