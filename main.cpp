#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <stdexcept>

#include "grafo.hpp"
#include "grupo.hpp"
#include "solucaoInicial.hpp"
#include "metodoLH.hpp"
#include "metodoGC.hpp"
#include "metodoWJ.hpp"
#include "buscaLocal.hpp"
#include "perturbacao.hpp"

using namespace std;


/*
    Os algoritmos de solução inicial utilizados:
        - LH: explicada no tópico 2.3 do artigo "Iterated maxima search for the maximally diverse grouping problem", disponível em https://doi.org/10.1016/j.ejor.2016.05.018
        - GC: pseudo-código na "Figure 5" do artigo "Tabu search with strategic oscillation for the
        maximally diverse grouping problem", disponível em https://doi.org/10.1057/jors.2012.66
        - WJ: pseudo-código na "Figure 3" do artigo "Tabu search with strategic oscillation for the
        maximally diverse grouping problem", disponível em https://doi.org/10.1057/jors.2012.66

    Os algoritmos de busca local (inserção e swap) foram implementados com base nas definições presentes em "Iterated maxima search for the maximally diverse grouping problem" (diponível em https://doi.org/10.1016/j.ejor.2016.05.018), e ambos os citados anteriorme, mais o 3-chain (swap em cadeia) também são encontrados em "Solving the maximally diverse grouping problem by skewed general variable neighborhood search" (disponível em https://doi.org/10.1016/j.ins.2014.10.043)

    Os algoritmos de perturbação utilizados:
        - Perturbação fraca e perturbação forte, "Algorithm 4" e "Algorithm 5", respectivamente, do artigo "Iterated maxima search for the maximally diverse grouping problem", disponível em https://doi.org/10.1016/j.ejor.2016.05.018
*/

int qtdIteracoesTotalPorTempo = 0;

void imprimeInformacoesDeUmGrupo(vector<Grupo> &solucao) {
    for (unsigned int i = 0; i < solucao.size(); ++i) {
        cout << "\nGRUPO " << (i + 1)
            << "\nlimiteInferior: " << solucao[i].getLimiteInferior()
            << "\nlimiteSuperior: " << solucao[i].getLimiteSuperior()
            << "\nqtdElementos: " << solucao[i].getQtdElementos()
            << "\nelementos: ";
            
        for (unsigned int j = 0; j < solucao[i].getElementos().size(); ++j) {
            cout << solucao[i].getElemento(j) << " ";
        }
        
        cout << "\narestas: ";
        vector<vector<int> >  arestasElementos = solucao[i].getArestasElementos();
        vector<double> arestasValor = solucao[i].getArestasValor();
        for (unsigned int j = 0; j < arestasElementos.size(); ++j) {
            cout << arestasElementos[j][0] << ", "
                << arestasElementos[j][1] << ", "
                << arestasValor[j] << "; ";
        }
        cout << "\nsomatorioDistancias: " << solucao[i].getSomatorioDistancias() << "\n";
    }
}


void realizaSomatorioDaSolucao(double &somatorioTotal, vector<Grupo> &solucao) {
    somatorioTotal = 0;
    // imprimeInformacoesDeUmGrupo(solucao);
    for (unsigned int i = 0; i  < solucao.size(); ++i) {
        somatorioTotal += solucao[i].getSomatorioDistancias();
    }
}


vector<Grupo> solucaoInicialLH(string &nomeArquivo, Grafo* &grafo) {
    // cout << "\n\n---------- Metodo LH: ----------\n\n";
    
    int qtdSolucoesFactiveis = 10;

    MetodoLH* solucaoInicial = new MetodoLH(qtdSolucoesFactiveis);

    grafo = solucaoInicial->leArquivo(nomeArquivo);
    
    vector<Grupo> solucao = solucaoInicial->gerarSolucao(grafo);

    // imprimeInformacoesDeUmGrupo(solucao);
    
    return solucao;
}


vector<Grupo> solucaoInicialGC(string &nomeArquivo, Grafo* &grafo) {
    // cout << "\n\n---------- Metodo GC: ----------\n\n";

    MetodoGC* solucaoInicial = new MetodoGC();
    
    grafo = solucaoInicial->leArquivo(nomeArquivo);
    
    vector<Grupo> solucao = solucaoInicial->gerarSolucao(grafo);

    // imprimeInformacoesDeUmGrupo(solucao);

    return solucao;
}


vector<Grupo> solucaoInicialWJ(string &nomeArquivo, Grafo* &grafo) {
    // cout << "\n\n---------- Metodo WJ: ----------\n\n";

    MetodoWJ* solucaoInicial = new MetodoWJ();
    
    grafo = solucaoInicial->leArquivo(nomeArquivo);
    
    vector<Grupo> solucao = solucaoInicial->gerarSolucao(grafo);

    // imprimeInformacoesDeUmGrupo(solucao);

    return solucao;
}


void gerarSolucaoInicial(string solucaoInicial, vector<Grupo> &solucao, string nomeArquivo,
                         Grafo* &grafo) {
    if (solucaoInicial == "lh") {
        solucao = solucaoInicialLH(nomeArquivo, grafo);
    }
    else if (solucaoInicial == "gc") {
        solucao = solucaoInicialGC(nomeArquivo, grafo);
    }
    else if (solucaoInicial == "wj") {
        solucao = solucaoInicialWJ(nomeArquivo, grafo);
    }
}


bool verificaCriterioDeAceitacao(string criterioDeAceitacao, vector<Grupo> solucao,
                               vector<Grupo> solucaoPer) {
    double somatorioSolucao = 0, somatorioSolucaoPer = 0;
    realizaSomatorioDaSolucao(somatorioSolucao, solucao);
    realizaSomatorioDaSolucao(somatorioSolucaoPer, solucaoPer);

    if (criterioDeAceitacao == "acceptBetter") {
        if (somatorioSolucaoPer > somatorioSolucao) {
            return true;
        }
    }
    else if (criterioDeAceitacao == "acceptBetterEqual") {
        if (somatorioSolucaoPer >= somatorioSolucao) {
            return true;
        }
    }
    else {
        return true;
    }

    return false;
}


double buscaLocalIterada(string &nomeArquivo, string &tipoSolucaoInicial,
                                string &tipoPerturbacao, string &buscaLocal, string &buscaLocalProx,
                                string &buscaLocalPer, string &buscaLocalPerProx,
                                string &criterioDeAceitacao) {

    Grafo* grafo;

    vector<Grupo> solucaoInicial;
    gerarSolucaoInicial(tipoSolucaoInicial, solucaoInicial, nomeArquivo, grafo);

    int qtdIteracoes = (grafo->getQtdElementos() / grafo->getQtdGrupos());
    Perturbacao perturbacao(tipoPerturbacao, buscaLocalPer, buscaLocalPerProx, qtdIteracoes);
    BuscaLocal busca(grafo, buscaLocal, buscaLocalProx);

    vector<Grupo> solucao = busca.busca(grafo, solucaoInicial);

    time_t tempo;

    if (grafo->getQtdElementos() == 120) {
        tempo = 10;
    }
    else if (grafo->getQtdElementos() == 240) {
        tempo = 60;
    }
    else if (grafo->getQtdElementos() == 480) {
        tempo = 120;
    }
    else {
        tempo = 180;
    }

    time_t tempoInicial = time(NULL);
    time_t tempoFinal;

    vector<Grupo> solucaoPer;

    do {
        solucaoPer = perturbacao.perturbacao(grafo, solucao, busca);

        solucaoPer = busca.busca(grafo, solucaoPer);

        if (verificaCriterioDeAceitacao(criterioDeAceitacao, solucao, solucaoPer)) {
            solucao = solucaoPer;
        }

        tempoFinal = time(NULL);
    } while (difftime(tempoFinal, tempoInicial) <= tempo);

    double somatorioTotal = 0;
    realizaSomatorioDaSolucao(somatorioTotal, solucao);

    return somatorioTotal;
}


int main(int argc, char *argv[]) {
    // Para gerar o executável para rodar o Irace

    string nomeArquivo = argv[2];

    string solucaoInicial(argv[6]);
    string tipoPerturbacao(argv[8]);
    string buscaLocal(argv[10]), buscaLocalProx(argv[12]);
    string buscaLocalPer(argv[14]), buscaLocalPerProx(argv[16]);
    string criterioDeAceitacao(argv[18]);

    cout << buscaLocalIterada(nomeArquivo, solucaoInicial, tipoPerturbacao, buscaLocal, buscaLocalProx, buscaLocalPer, buscaLocalPerProx, criterioDeAceitacao);

    return 0;
}